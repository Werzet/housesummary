﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HouseSummary
{
    class TableRecord
    {
        public string RoomNumber { get; set; }
        public string MeasurePointName { get; set; }
        public string AutoPoll { get; set; }
        public string DataAvailable { get; set; }
        public string LastData { get; set; }
        public string LastIntegratorData { get; set; }
        public string EquipmentID { get; set; }
        public string EquipmentChannel { get; set; }
        public string ModulID { get; set; }
        public string BattaryDate { get; set; }
        public string ConsumptionPer { get; set; }
        public string ConsumptionPerMounth { get; set; }


        public TableRecord()
        { }
        
        public TableRecord(string RoomNumber, string MeasurePointName, string autoPoll, string DataAvailable, string LastData,
            string LastIntegratorData, string EquipmentID, string EquipmentChannel, string ModulID,
            string BattaryDate, string ConsumptionPer, string ConsumptionPerMounth)
        {
            this.RoomNumber = RoomNumber;
            this.MeasurePointName = MeasurePointName;
            this.AutoPoll = autoPoll;
            this.DataAvailable = DataAvailable;
            this.LastData = LastData;
            this.LastIntegratorData = LastIntegratorData;
            this.EquipmentID = EquipmentID;
            this.EquipmentChannel = EquipmentChannel;
            this.ModulID = ModulID;
            this.BattaryDate = BattaryDate;
            this.ConsumptionPer = ConsumptionPer;
            this.ConsumptionPerMounth = ConsumptionPerMounth;
        }

    }
}
