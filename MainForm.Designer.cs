﻿namespace HouseSummary
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.AvaliableData = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ConsumptionFor = new DevExpress.XtraEditors.ComboBoxEdit();
            this.ConsumptionFrom = new DevExpress.XtraEditors.ComboBoxEdit();
            this.gridColumn1 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.gridColumn2 = new DevExpress.XtraGrid.Columns.GridColumn();
            this.UpdateButton = new DevExpress.XtraEditors.SimpleButton();
            this.RoomsList = new DevExpress.XtraEditors.CheckedListBoxControl();
            this.CheckAll = new DevExpress.XtraEditors.CheckEdit();
            this.groupControl1 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl2 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl3 = new DevExpress.XtraEditors.GroupControl();
            this.groupControl4 = new DevExpress.XtraEditors.GroupControl();
            this.simpleButton1 = new DevExpress.XtraEditors.SimpleButton();
            this.MainTableView = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.RoomNumber = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MeasurePointName = new DevExpress.XtraGrid.Columns.GridColumn();
            this.AutoPoll = new DevExpress.XtraGrid.Columns.GridColumn();
            this.DataAvailable = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LastData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.LastIntegratorData = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EquipmentID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.EquipmentChannel = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ModulID = new DevExpress.XtraGrid.Columns.GridColumn();
            this.BattaryDate = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ConsumptionPer = new DevExpress.XtraGrid.Columns.GridColumn();
            this.ConsumptionPerMounth = new DevExpress.XtraGrid.Columns.GridColumn();
            this.MainTable = new DevExpress.XtraGrid.GridControl();
            this.groupControl5 = new DevExpress.XtraEditors.GroupControl();
            this.labelControl7 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl8 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl5 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl6 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl3 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl4 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl2 = new DevExpress.XtraEditors.LabelControl();
            this.labelControl1 = new DevExpress.XtraEditors.LabelControl();
            ((System.ComponentModel.ISupportInitialize)(this.AvaliableData.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConsumptionFor.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConsumptionFrom.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoomsList)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckAll.Properties)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).BeginInit();
            this.groupControl1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).BeginInit();
            this.groupControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).BeginInit();
            this.groupControl3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).BeginInit();
            this.groupControl4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.MainTableView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).BeginInit();
            this.groupControl5.SuspendLayout();
            this.SuspendLayout();
            // 
            // AvaliableData
            // 
            this.AvaliableData.EditValue = "1 день";
            this.AvaliableData.Location = new System.Drawing.Point(5, 23);
            this.AvaliableData.Name = "AvaliableData";
            this.AvaliableData.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.AvaliableData.Properties.Items.AddRange(new object[] {
            "1 день",
            "2 дня",
            "3 дня",
            "4 дня",
            "5 дней",
            "6 дней",
            "7 дней",
            "8 дней",
            "9 дней",
            "10 дней",
            "11 дней",
            "12 дней",
            "13 дней",
            "14 дней",
            "15 дней"});
            this.AvaliableData.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.AvaliableData.Size = new System.Drawing.Size(190, 20);
            this.AvaliableData.TabIndex = 3;
            this.AvaliableData.SelectedIndexChanged += new System.EventHandler(this.AvaliableData_SelectedIndexChanged);
            // 
            // ConsumptionFor
            // 
            this.ConsumptionFor.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ConsumptionFor.EditValue = "15 последних дней";
            this.ConsumptionFor.Location = new System.Drawing.Point(5, 23);
            this.ConsumptionFor.Name = "ConsumptionFor";
            this.ConsumptionFor.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ConsumptionFor.Properties.Items.AddRange(new object[] {
            "15 последних дней",
            "14 последних дней",
            "13 последних дней",
            "12 последних дней",
            "11 последних дней",
            "10 последних дней",
            "9 последних дней",
            "8 последних дней",
            "7 последних дней",
            "6 последних дней",
            "5 последних дней",
            "4 последних дня",
            "3 последних дня",
            "2 последних дня",
            "последний день"});
            this.ConsumptionFor.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ConsumptionFor.Size = new System.Drawing.Size(190, 20);
            this.ConsumptionFor.TabIndex = 5;
            this.ConsumptionFor.SelectedIndexChanged += new System.EventHandler(this.ConsumptionForLastData_SelectedIndexChanged);
            // 
            // ConsumptionFrom
            // 
            this.ConsumptionFrom.EditValue = "с начала месяца";
            this.ConsumptionFrom.Location = new System.Drawing.Point(5, 23);
            this.ConsumptionFrom.Name = "ConsumptionFrom";
            this.ConsumptionFrom.Properties.Buttons.AddRange(new DevExpress.XtraEditors.Controls.EditorButton[] {
            new DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)});
            this.ConsumptionFrom.Properties.Items.AddRange(new object[] {
            "с начала месяца"});
            this.ConsumptionFrom.Properties.TextEditStyle = DevExpress.XtraEditors.Controls.TextEditStyles.DisableTextEditor;
            this.ConsumptionFrom.Size = new System.Drawing.Size(190, 20);
            this.ConsumptionFrom.TabIndex = 6;
            this.ConsumptionFrom.SelectedIndexChanged += new System.EventHandler(this.ConsumptionFrom_SelectedIndexChanged);
            // 
            // gridColumn1
            // 
            this.gridColumn1.Caption = "Наименование точки учёта";
            this.gridColumn1.Name = "gridColumn1";
            this.gridColumn1.Visible = true;
            this.gridColumn1.VisibleIndex = 0;
            this.gridColumn1.Width = 94;
            // 
            // gridColumn2
            // 
            this.gridColumn2.Caption = "Наименование точки учёта";
            this.gridColumn2.Name = "gridColumn2";
            this.gridColumn2.Visible = true;
            this.gridColumn2.VisibleIndex = 0;
            this.gridColumn2.Width = 94;
            // 
            // UpdateButton
            // 
            this.UpdateButton.Location = new System.Drawing.Point(12, 511);
            this.UpdateButton.Name = "UpdateButton";
            this.UpdateButton.Size = new System.Drawing.Size(117, 23);
            this.UpdateButton.TabIndex = 10;
            this.UpdateButton.Text = "Собрать сведения";
            this.UpdateButton.Click += new System.EventHandler(this.UpdateButton_Click);
            // 
            // RoomsList
            // 
            this.RoomsList.CheckOnClick = true;
            this.RoomsList.Location = new System.Drawing.Point(5, 48);
            this.RoomsList.Name = "RoomsList";
            this.RoomsList.SelectionMode = System.Windows.Forms.SelectionMode.None;
            this.RoomsList.Size = new System.Drawing.Size(190, 272);
            this.RoomsList.TabIndex = 12;
            // 
            // CheckAll
            // 
            this.CheckAll.Location = new System.Drawing.Point(5, 23);
            this.CheckAll.Name = "CheckAll";
            this.CheckAll.Properties.Caption = "Выбрать всё";
            this.CheckAll.Size = new System.Drawing.Size(105, 19);
            this.CheckAll.TabIndex = 13;
            this.CheckAll.CheckedChanged += new System.EventHandler(this.checkAll_CheckedChanged);
            // 
            // groupControl1
            // 
            this.groupControl1.Controls.Add(this.RoomsList);
            this.groupControl1.Controls.Add(this.CheckAll);
            this.groupControl1.Location = new System.Drawing.Point(12, 180);
            this.groupControl1.Name = "groupControl1";
            this.groupControl1.Size = new System.Drawing.Size(200, 325);
            this.groupControl1.TabIndex = 16;
            this.groupControl1.Text = "Список помещений";
            // 
            // groupControl2
            // 
            this.groupControl2.Controls.Add(this.AvaliableData);
            this.groupControl2.Location = new System.Drawing.Point(12, 12);
            this.groupControl2.Name = "groupControl2";
            this.groupControl2.Size = new System.Drawing.Size(200, 49);
            this.groupControl2.TabIndex = 17;
            this.groupControl2.Text = "Наличие данных за";
            // 
            // groupControl3
            // 
            this.groupControl3.Controls.Add(this.ConsumptionFor);
            this.groupControl3.Location = new System.Drawing.Point(12, 68);
            this.groupControl3.Name = "groupControl3";
            this.groupControl3.Size = new System.Drawing.Size(200, 49);
            this.groupControl3.TabIndex = 18;
            this.groupControl3.Text = "Потребление за последнии дни";
            // 
            // groupControl4
            // 
            this.groupControl4.Controls.Add(this.ConsumptionFrom);
            this.groupControl4.Location = new System.Drawing.Point(12, 124);
            this.groupControl4.Name = "groupControl4";
            this.groupControl4.Size = new System.Drawing.Size(200, 50);
            this.groupControl4.TabIndex = 19;
            this.groupControl4.Text = "Потребление";
            // 
            // simpleButton1
            // 
            this.simpleButton1.ButtonStyle = DevExpress.XtraEditors.Controls.BorderStyles.HotFlat;
            this.simpleButton1.Image = ((System.Drawing.Image)(resources.GetObject("simpleButton1.Image")));
            this.simpleButton1.ImageLocation = DevExpress.XtraEditors.ImageLocation.MiddleCenter;
            this.simpleButton1.Location = new System.Drawing.Point(186, 512);
            this.simpleButton1.Name = "simpleButton1";
            this.simpleButton1.Size = new System.Drawing.Size(26, 22);
            this.simpleButton1.TabIndex = 20;
            this.simpleButton1.Click += new System.EventHandler(this.simpleButton1_Click);
            // 
            // MainTableView
            // 
            this.MainTableView.Appearance.HeaderPanel.Options.UseTextOptions = true;
            this.MainTableView.Appearance.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MainTableView.AppearancePrint.HeaderPanel.Options.UseTextOptions = true;
            this.MainTableView.AppearancePrint.HeaderPanel.TextOptions.Trimming = DevExpress.Utils.Trimming.Word;
            this.MainTableView.AppearancePrint.HeaderPanel.TextOptions.WordWrap = DevExpress.Utils.WordWrap.Wrap;
            this.MainTableView.Columns.AddRange(new DevExpress.XtraGrid.Columns.GridColumn[] {
            this.RoomNumber,
            this.MeasurePointName,
            this.AutoPoll,
            this.DataAvailable,
            this.LastData,
            this.LastIntegratorData,
            this.EquipmentID,
            this.EquipmentChannel,
            this.ModulID,
            this.BattaryDate,
            this.ConsumptionPer,
            this.ConsumptionPerMounth});
            this.MainTableView.GridControl = this.MainTable;
            this.MainTableView.Name = "MainTableView";
            this.MainTableView.OptionsBehavior.Editable = false;
            this.MainTableView.OptionsPrint.UsePrintStyles = false;
            this.MainTableView.OptionsView.ColumnHeaderAutoHeight = DevExpress.Utils.DefaultBoolean.True;
            this.MainTableView.OptionsView.ShowAutoFilterRow = true;
            this.MainTableView.OptionsView.ShowFilterPanelMode = DevExpress.XtraGrid.Views.Base.ShowFilterPanelMode.Never;
            this.MainTableView.RowStyle += new DevExpress.XtraGrid.Views.Grid.RowStyleEventHandler(this.MainTableView_RowStyle);
            this.MainTableView.CustomColumnSort += new DevExpress.XtraGrid.Views.Base.CustomColumnSortEventHandler(this.MainTableView_CustomColumnSort);
            // 
            // RoomNumber
            // 
            this.RoomNumber.Caption = "Номер квартиры";
            this.RoomNumber.FieldName = "RoomNumber";
            this.RoomNumber.Name = "RoomNumber";
            this.RoomNumber.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.RoomNumber.Visible = true;
            this.RoomNumber.VisibleIndex = 0;
            // 
            // MeasurePointName
            // 
            this.MeasurePointName.Caption = "Наименование точки учёта";
            this.MeasurePointName.FieldName = "MeasurePointName";
            this.MeasurePointName.Name = "MeasurePointName";
            this.MeasurePointName.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.MeasurePointName.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.MeasurePointName.Visible = true;
            this.MeasurePointName.VisibleIndex = 1;
            this.MeasurePointName.Width = 94;
            // 
            // AutoPoll
            // 
            this.AutoPoll.Caption = "Авто опрос";
            this.AutoPoll.FieldName = "AutoPoll";
            this.AutoPoll.Name = "AutoPoll";
            this.AutoPoll.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.AutoPoll.Visible = true;
            this.AutoPoll.VisibleIndex = 2;
            this.AutoPoll.Width = 69;
            // 
            // DataAvailable
            // 
            this.DataAvailable.Caption = "Наличие данных за ";
            this.DataAvailable.FieldName = "DataAvailable";
            this.DataAvailable.Name = "DataAvailable";
            this.DataAvailable.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.DataAvailable.Visible = true;
            this.DataAvailable.VisibleIndex = 3;
            // 
            // LastData
            // 
            this.LastData.Caption = "Дата и время снятия последних показаний";
            this.LastData.FieldName = "LastData";
            this.LastData.Name = "LastData";
            this.LastData.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.LastData.Visible = true;
            this.LastData.VisibleIndex = 4;
            this.LastData.Width = 68;
            // 
            // LastIntegratorData
            // 
            this.LastIntegratorData.Caption = "Последние снятые показания интегратора";
            this.LastIntegratorData.FieldName = "LastIntegratorData";
            this.LastIntegratorData.Name = "LastIntegratorData";
            this.LastIntegratorData.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.LastIntegratorData.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.LastIntegratorData.Visible = true;
            this.LastIntegratorData.VisibleIndex = 5;
            this.LastIntegratorData.Width = 83;
            // 
            // EquipmentID
            // 
            this.EquipmentID.Caption = "Номер счётчика";
            this.EquipmentID.FieldName = "EquipmentID";
            this.EquipmentID.Name = "EquipmentID";
            this.EquipmentID.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.EquipmentID.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.EquipmentID.Visible = true;
            this.EquipmentID.VisibleIndex = 6;
            this.EquipmentID.Width = 65;
            // 
            // EquipmentChannel
            // 
            this.EquipmentChannel.Caption = "Канал";
            this.EquipmentChannel.FieldName = "EquipmentChannel";
            this.EquipmentChannel.Name = "EquipmentChannel";
            this.EquipmentChannel.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.EquipmentChannel.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.EquipmentChannel.Visible = true;
            this.EquipmentChannel.VisibleIndex = 7;
            this.EquipmentChannel.Width = 54;
            // 
            // ModulID
            // 
            this.ModulID.Caption = "Номер квартирного радиомодуля";
            this.ModulID.FieldName = "ModulID";
            this.ModulID.Name = "ModulID";
            this.ModulID.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.ModulID.UnboundType = DevExpress.Data.UnboundColumnType.String;
            this.ModulID.Visible = true;
            this.ModulID.VisibleIndex = 8;
            this.ModulID.Width = 76;
            // 
            // BattaryDate
            // 
            this.BattaryDate.Caption = "Дата установки (замены) батарейки";
            this.BattaryDate.FieldName = "BattaryDate";
            this.BattaryDate.Name = "BattaryDate";
            this.BattaryDate.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.BattaryDate.Visible = true;
            this.BattaryDate.VisibleIndex = 9;
            this.BattaryDate.Width = 63;
            // 
            // ConsumptionPer
            // 
            this.ConsumptionPer.Caption = "Потребление за ";
            this.ConsumptionPer.FieldName = "ConsumptionPer";
            this.ConsumptionPer.Name = "ConsumptionPer";
            this.ConsumptionPer.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.ConsumptionPer.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.ConsumptionPer.Visible = true;
            this.ConsumptionPer.VisibleIndex = 10;
            this.ConsumptionPer.Width = 81;
            // 
            // ConsumptionPerMounth
            // 
            this.ConsumptionPerMounth.Caption = "Потребление ";
            this.ConsumptionPerMounth.FieldName = "ConsumptionPerMounth";
            this.ConsumptionPerMounth.Name = "ConsumptionPerMounth";
            this.ConsumptionPerMounth.SortMode = DevExpress.XtraGrid.ColumnSortMode.Custom;
            this.ConsumptionPerMounth.UnboundType = DevExpress.Data.UnboundColumnType.Object;
            this.ConsumptionPerMounth.Visible = true;
            this.ConsumptionPerMounth.VisibleIndex = 11;
            this.ConsumptionPerMounth.Width = 85;
            // 
            // MainTable
            // 
            this.MainTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MainTable.Location = new System.Drawing.Point(218, 12);
            this.MainTable.MainView = this.MainTableView;
            this.MainTable.Name = "MainTable";
            this.MainTable.Size = new System.Drawing.Size(826, 662);
            this.MainTable.TabIndex = 0;
            this.MainTable.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.MainTableView});
            // 
            // groupControl5
            // 
            this.groupControl5.Controls.Add(this.labelControl7);
            this.groupControl5.Controls.Add(this.labelControl8);
            this.groupControl5.Controls.Add(this.labelControl5);
            this.groupControl5.Controls.Add(this.labelControl6);
            this.groupControl5.Controls.Add(this.labelControl3);
            this.groupControl5.Controls.Add(this.labelControl4);
            this.groupControl5.Controls.Add(this.labelControl2);
            this.groupControl5.Controls.Add(this.labelControl1);
            this.groupControl5.Location = new System.Drawing.Point(12, 540);
            this.groupControl5.Name = "groupControl5";
            this.groupControl5.Size = new System.Drawing.Size(200, 134);
            this.groupControl5.TabIndex = 21;
            this.groupControl5.Text = "Легенда цветов";
            // 
            // labelControl7
            // 
            this.labelControl7.Location = new System.Drawing.Point(25, 107);
            this.labelControl7.Name = "labelControl7";
            this.labelControl7.Size = new System.Drawing.Size(115, 13);
            this.labelControl7.TabIndex = 7;
            this.labelControl7.Text = "- ошибка при экспорте";
            // 
            // labelControl8
            // 
            this.labelControl8.Appearance.BackColor = System.Drawing.Color.SeaGreen;
            this.labelControl8.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl8.Location = new System.Drawing.Point(5, 106);
            this.labelControl8.Name = "labelControl8";
            this.labelControl8.Size = new System.Drawing.Size(14, 15);
            this.labelControl8.TabIndex = 6;
            this.labelControl8.Text = "    ";
            // 
            // labelControl5
            // 
            this.labelControl5.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl5.Location = new System.Drawing.Point(25, 75);
            this.labelControl5.Name = "labelControl5";
            this.labelControl5.Size = new System.Drawing.Size(170, 26);
            this.labelControl5.TabIndex = 5;
            this.labelControl5.Text = "- нет потребления с указанного числа";
            // 
            // labelControl6
            // 
            this.labelControl6.Appearance.BackColor = System.Drawing.Color.Salmon;
            this.labelControl6.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl6.Location = new System.Drawing.Point(5, 74);
            this.labelControl6.Name = "labelControl6";
            this.labelControl6.Size = new System.Drawing.Size(14, 15);
            this.labelControl6.TabIndex = 4;
            this.labelControl6.Text = "    ";
            // 
            // labelControl3
            // 
            this.labelControl3.AutoSizeMode = DevExpress.XtraEditors.LabelAutoSizeMode.Vertical;
            this.labelControl3.Location = new System.Drawing.Point(25, 43);
            this.labelControl3.Name = "labelControl3";
            this.labelControl3.Size = new System.Drawing.Size(170, 26);
            this.labelControl3.TabIndex = 3;
            this.labelControl3.Text = "- нет потребления за последнии дни";
            // 
            // labelControl4
            // 
            this.labelControl4.Appearance.BackColor = System.Drawing.Color.SandyBrown;
            this.labelControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl4.Location = new System.Drawing.Point(5, 42);
            this.labelControl4.Name = "labelControl4";
            this.labelControl4.Size = new System.Drawing.Size(14, 15);
            this.labelControl4.TabIndex = 2;
            this.labelControl4.Text = "    ";
            // 
            // labelControl2
            // 
            this.labelControl2.Location = new System.Drawing.Point(25, 24);
            this.labelControl2.Name = "labelControl2";
            this.labelControl2.Size = new System.Drawing.Size(135, 13);
            this.labelControl2.TabIndex = 1;
            this.labelControl2.Text = "- просроченная батарейка";
            // 
            // labelControl1
            // 
            this.labelControl1.Appearance.BackColor = System.Drawing.Color.Khaki;
            this.labelControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.Simple;
            this.labelControl1.Location = new System.Drawing.Point(5, 23);
            this.labelControl1.Name = "labelControl1";
            this.labelControl1.Size = new System.Drawing.Size(14, 15);
            this.labelControl1.TabIndex = 0;
            this.labelControl1.Text = "    ";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1056, 686);
            this.Controls.Add(this.groupControl5);
            this.Controls.Add(this.simpleButton1);
            this.Controls.Add(this.groupControl4);
            this.Controls.Add(this.groupControl3);
            this.Controls.Add(this.groupControl2);
            this.Controls.Add(this.groupControl1);
            this.Controls.Add(this.UpdateButton);
            this.Controls.Add(this.MainTable);
            this.Name = "MainForm";
            this.Text = "Сводка по дому";
            ((System.ComponentModel.ISupportInitialize)(this.AvaliableData.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConsumptionFor.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ConsumptionFrom.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RoomsList)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CheckAll.Properties)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl1)).EndInit();
            this.groupControl1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl2)).EndInit();
            this.groupControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl3)).EndInit();
            this.groupControl3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.groupControl4)).EndInit();
            this.groupControl4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.MainTableView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MainTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.groupControl5)).EndInit();
            this.groupControl5.ResumeLayout(false);
            this.groupControl5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private DevExpress.XtraEditors.ComboBoxEdit AvaliableData;
        private DevExpress.XtraEditors.ComboBoxEdit ConsumptionFor;
        private DevExpress.XtraEditors.ComboBoxEdit ConsumptionFrom;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn1;
        private DevExpress.XtraGrid.Columns.GridColumn gridColumn2;
        private DevExpress.XtraEditors.SimpleButton UpdateButton;
        private DevExpress.XtraEditors.CheckedListBoxControl RoomsList;
        private DevExpress.XtraEditors.CheckEdit CheckAll;
        private DevExpress.XtraEditors.GroupControl groupControl1;
        private DevExpress.XtraEditors.GroupControl groupControl2;
        private DevExpress.XtraEditors.GroupControl groupControl3;
        private DevExpress.XtraEditors.GroupControl groupControl4;
        private DevExpress.XtraEditors.SimpleButton simpleButton1;
        private DevExpress.XtraGrid.Views.Grid.GridView MainTableView;
        private DevExpress.XtraGrid.Columns.GridColumn RoomNumber;
        private DevExpress.XtraGrid.Columns.GridColumn MeasurePointName;
        private DevExpress.XtraGrid.Columns.GridColumn AutoPoll;
        private DevExpress.XtraGrid.Columns.GridColumn DataAvailable;
        private DevExpress.XtraGrid.Columns.GridColumn LastData;
        private DevExpress.XtraGrid.Columns.GridColumn LastIntegratorData;
        private DevExpress.XtraGrid.Columns.GridColumn EquipmentID;
        private DevExpress.XtraGrid.Columns.GridColumn EquipmentChannel;
        private DevExpress.XtraGrid.Columns.GridColumn ModulID;
        private DevExpress.XtraGrid.Columns.GridColumn BattaryDate;
        private DevExpress.XtraGrid.Columns.GridColumn ConsumptionPer;
        private DevExpress.XtraGrid.Columns.GridColumn ConsumptionPerMounth;
        private DevExpress.XtraGrid.GridControl MainTable;
        private DevExpress.XtraEditors.GroupControl groupControl5;
        private DevExpress.XtraEditors.LabelControl labelControl1;
        private DevExpress.XtraEditors.LabelControl labelControl7;
        private DevExpress.XtraEditors.LabelControl labelControl8;
        private DevExpress.XtraEditors.LabelControl labelControl5;
        private DevExpress.XtraEditors.LabelControl labelControl6;
        private DevExpress.XtraEditors.LabelControl labelControl3;
        private DevExpress.XtraEditors.LabelControl labelControl4;
        private DevExpress.XtraEditors.LabelControl labelControl2;
    }
}

