﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lers.Plugins;
using Lers.UI;
using Lers.UI.Tabs;
using Lers.Core;
using Lers.Communal;
using DevExpress.XtraEditors;
using System.Windows.Forms;

namespace HouseSummary
{
    class Plugin : IPlugin
    {
        private List<MainForm> visibleForms = new List<MainForm>();

        /// <summary>
        /// Экземпляр хост-интерфейса клиента
        /// </summary>
        internal static IPluginHost Host
        { get; private set; }

        /// <summary>
		/// Инициализация.
		/// </summary>
		/// <param name="host"></param>
		public void Initialize(IPluginHost host)
        {
            Plugin.Host = host;
            
            Plugin.Host.MainWindow.RegisterObjectAction(ObjectType.Node, "Собрать сводку по жилому дому", Properties.Resources.ico, OnHouseSummaryOpen);
        }

        /// <summary>
        /// Пользователь выбрал пункт меню
        /// </summary>
        /// <param name="actionId"></param>
        /// <param name="sender"></param>
        private void OnHouseSummaryOpen(int actionId, object sender)
        {
            if(sender is House)
            {
                // Проверим, открыто ли окно со сводкой по дому
                MainForm currentForm = GetOpenedForm((House)sender);

                if (currentForm == null)
                {
                    // Такого окна нет, открываем
                    NewForm((House)sender);
                }

                else
                {
                    // Окно уже есть, переводим фокус на него
                    currentForm.Show();
                    currentForm.Focus();
                }
            }

            else
            {
                XtraMessageBox.Show("Можно собрать сводку только по жилому дому",
                    "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return;
            }
        }

        /// <summary>
        /// Открывает новое окно 
        /// </summary>
        /// <param name="device"></param>
        private void NewForm(House house)
        {
            // Окно для этого дома ещё не открыто
            MainForm mainForm = new MainForm();
            
            // Заголовок окна
            mainForm.Text = "Сводка по дому - " + house.Title;

            // Инициализация
            mainForm.Initialize(house, Plugin.Host);

            Plugin.Host.MainWindow.AddPage(mainForm);

            mainForm.Show();

            mainForm.FormClosed += new System.Windows.Forms.FormClosedEventHandler(currentForm_FormClosed);

            lock (this.visibleForms)
            {
                this.visibleForms.Add(mainForm);
            }
        }

        /// <summary>
		/// Закрыто окно удалённого пульта. Удаляем его из списка открытых окон.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void currentForm_FormClosed(object sender, System.Windows.Forms.FormClosedEventArgs e)
        {
            lock (this.visibleForms)
            {
                this.visibleForms.Remove((MainForm)sender);
            }
        }

        
        /// <summary>
        /// Возвращает открытую форму, работающую с указанным устройством, или null если формы нет
        /// </summary>
        /// <param name="device"></param>
        /// <returns></returns>
        private MainForm GetOpenedForm(House house)
        {
            lock (this.visibleForms)
            {
                foreach (MainForm form in this.visibleForms)
                {
                    if (form.CurrentHouse == house)
                    {
                        return form;
                    }
                }
            }

            return null;
        }
        
        /*
        public void menu_ItemClick(object sender, EventArgs e)
        {

            Plugin.Host = this.pluginHost.Server;

            MainForm form = new MainForm();

            //form.Owner = PluginImplementation.Host.MainWindow.ActiveForm;

            form.Show(pluginHost.MainWindow.ActiveForm);
        }*/


    }
}
