﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lers;
using Lers.UI.Tabs;
using Lers.Communal;
using Lers.Core;
using Lers.Data;
using DevExpress.XtraEditors;
using Lers.UI.Indicators;
using Lers.Plugins;
using DevExpress.XtraEditors.Controls;
using DevExpress.XtraGrid.Views.Grid;
using DevExpress.XtraPrinting;

namespace HouseSummary
{
    public partial class MainForm : DevExpress.XtraEditors.XtraForm
    {
        public House CurrentHouse { get; private set; }

        BindingList<TableRecord> listDataSource = new BindingList<TableRecord>();

        private ExportManager exporter;       

        BusyIndicatorControl busyControl;

        //LersServer server;

        IPluginHost host;

        /// <summary>
        /// Служит для перехода в основной поток, вызывает функцию SetResultImageDelegate
        /// </summary>
        /// <param name="node">Узел дерева, для которого устанавливаем результат</param>
        /// <param name="importStatus">Состояние импорта данных</param>
        /// <param name="text">Текст результата</param>
        private delegate void SetResultListDelegate(BindingList<TableRecord> records, string message);


        public MainForm()
        {
            InitializeComponent();

            busyControl = new BusyIndicatorControl(this);

            busyControl.CancelClick += new EventHandler(busyControl_CancelClick);

            exporter = new ExportManager(this);
            
            exporter.Completed += new ExportManager.CompletedEventHandler(exporter_Completed);
            exporter.ProgressIncremented += new ExportManager.ProgressIncrementedEventHandler(exporter_ProgressIncremented);
            exporter.ResultReceived += new ExportManager.ResultReceivedEventHandler(exporter_ResultReceived);
        }

        private void busyControl_CancelClick(object sender, EventArgs e)
        {
            exporter.Abort();
        }

        private void exporter_ResultReceived(object sender, ResultReceivedEventArgs e)
        {
            SetResult(e.records, e.Message);
        }

        private void SetResult(BindingList<TableRecord> records, string message)
        {           
            if (!MainTable.InvokeRequired)
            {
                if (message == "Успех")
                {
                    if (records != null)
                    {
                        busyControl.Message = "Сохранение результатов";

                        foreach (TableRecord record in records)
                            listDataSource.Add(record);
                    }
                }

                else
                {
                    ShowErrorMessageBox(message);

                    if(records!=null)
                        foreach (TableRecord record in records)
                            listDataSource.Add(record);
                }
            }
            else
            {
                SetResultListDelegate del = new SetResultListDelegate(SetResult);
                MainTable.Invoke(del, new object[] { records, message });
                
            }
           
        }

        private void exporter_ProgressIncremented(object sender, ProgressChangedEventArgs e)
        {
            busyControl.ProgressBarValue += e.ProgressPercentage;
            busyControl.Message = string.Format("Обработано {0} из {1} записей", busyControl.ProgressBarValue.ToString(), busyControl.ProgressBarMaximum.ToString());
        }

        private void exporter_Completed(object sender, RunWorkerCompletedEventArgs e)
        {
            exporter.StopExporter();
            busyControl.Hide();
            busyControl.ShowProgressBar = false;
            busyControl.ShowCancelButton = false;   
        }

        internal void Initialize(House house, IPluginHost host)
        {
            this.CurrentHouse = house;
            this.host = host;

            MainTableView.Columns[3].Caption = "Наличие данных за " + AvaliableData.Text;
            MainTableView.Columns[10].Caption = "Потребление за " + ConsumptionFor.Text;
            MainTableView.Columns[11].Caption = "Потребление " + ConsumptionFrom.Text;

            MainTable.DataSource = listDataSource;

            CurrentHouse.Refresh(NodeInfoFlags.Rooms);

            RoomsList.DataSource = CurrentHouse.Rooms.OrderBy(room => room.Title, new OrdinalStringComparer());

            FillConsumptionDate();
        }

        private void FillConsumptionDate()
        {
            DateTime today = DateTime.Today;

            for(int i=2; i<today.Day; i++)
            {
                if(i == 2)
                    ConsumptionFrom.Properties.Items.Add(string.Format("со {0} числа", i));
                else
                    ConsumptionFrom.Properties.Items.Add(string.Format("с {0} числа", i));
            }
        }

        private void AvaliableData_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainTableView.Columns[3].Caption = "Наличие данных за " + AvaliableData.Text;
        }

        private void ConsumptionForLastData_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainTableView.Columns[10].Caption = "Потребление за " + ConsumptionFor.Text;
        }

        private void ConsumptionFrom_SelectedIndexChanged(object sender, EventArgs e)
        {
            MainTableView.Columns[11].Caption = "Потребление " + ConsumptionFrom.Text;
        }

        private void UpdateButton_Click(object sender, EventArgs e)
        {
            try
            {
                RunExport();
            }
            catch(Exception exc)
            {
                exporter.Abort();
                busyControl.Hide();
                busyControl.ShowProgressBar = false;
                busyControl.ShowCancelButton = false;

                ShowErrorMessageBox(ExceptionHandler.HandleException(this, exc));
            }
        }

        private void ShowErrorMessageBox(string exc)
        {
            var dialogTypeName = "System.Windows.Forms.PropertyGridInternal.GridErrorDlg";
            var dialogType = typeof(Form).Assembly.GetType(dialogTypeName);

            // Create dialog instance.
            var dialog = (Form)Activator.CreateInstance(dialogType, new PropertyGrid());

            // Populate relevant properties on the dialog instance.
            dialog.Text = "Ошибка";
            dialogType.GetProperty("Details").SetValue(dialog, exc, null);
            dialogType.GetProperty("Message").SetValue(dialog, "Во время импорта произошли ошибки. Не все точки учёта были обработаны" , null);

            // Display dialog.
            var result = dialog.ShowDialog();
        }

        private void RunExport()
        {
            if(RoomsList.CheckedItemsCount == 0)
            {
                XtraMessageBox.Show(this, "Выбирите хотя бы одну квартиру", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
            }

            else
            {
                listDataSource.Clear();
                MainTable.Refresh();

                busyControl.ProgressBarMinimum = 0;
                busyControl.ProgressBarValue = 0;
                busyControl.Show("Подсчёт точек учёта");
                           
                List<MeasurePoint> measurePointList = FillMeasurePointList();                           

                busyControl.ProgressBarMaximum = measurePointList.ToArray().Length;
                busyControl.ShowProgressBar = true;
                busyControl.ShowCancelButton = true;
                
                int consumtionFrom = ConsumptionFrom.SelectedIndex + 1;

                string[] temp = ConsumptionFor.SelectedItem.ToString().Split(' ');

                int consumptionFor = Convert.ToInt32(temp[0]);

                exporter.Export(measurePointList, AvaliableData.SelectedIndex + 1, consumptionFor, consumtionFrom);
            }    
        }

        private List<MeasurePoint> FillMeasurePointList()
        {
            MeasurePoint[] serverList = host.Server.MeasurePoints.GetList(MeasurePointType.Communal, MeasurePointInfoFlags.Attributes | MeasurePointInfoFlags.Equipment);

            serverList = serverList.Where(mptitle => mptitle.FullTitle.Split('-')[0].Trim(' ').CompareTo(CurrentHouse.Title) == 0).ToArray();

            List<Room> roomList = RoomsList.CheckedItems.Cast<Room>().OrderBy(room => room.Title, new OrdinalStringComparer()).ToList();

            List<MeasurePoint> measurePointList = new List<MeasurePoint>();

            foreach (Room room in roomList)
            {
                measurePointList.AddRange(serverList.Where(name => name.FullTitle.Split('-')[1].Trim(' ').CompareTo(room.Title) == 0));
            }
            return measurePointList;
        }      

        private void checkAll_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckAll.Checked)
                RoomsList.CheckAll();
            else
                RoomsList.UnCheckAll();
        }

        private void MainTableView_RowStyle(object sender, RowStyleEventArgs e)
        {
            GridView View = sender as GridView;
            if (e.RowHandle >= 0)
            {

                string column = View.GetRowCellDisplayText(e.RowHandle, View.Columns["BattaryDate"]);

                if (column == "")
                {
                    e.Appearance.BackColor = Color.Khaki;
                    
                }

                column = View.GetRowCellDisplayText(e.RowHandle, View.Columns["ConsumptionPer"]);

                if (column == "нет данных" || column == "0")
                {
                    e.Appearance.BackColor = Color.SandyBrown;
                }

                column = View.GetRowCellDisplayText(e.RowHandle, View.Columns["ConsumptionPerMounth"]);

                if (column == "нет данных" || column == "0")
                {
                    e.Appearance.BackColor = Color.Salmon;
                }

                column = View.GetRowCellDisplayText(e.RowHandle, View.Columns["MeasurePointName"]);

                if (column == "")
                {
                    e.Appearance.BackColor = Color.SeaGreen;
                }

            }
        }

        private void simpleButton1_Click(object sender, EventArgs e)
        {
            // Окно для этого дома ещё не открыто
            PrintForm printForm = new PrintForm();

            // Заголовок окна
            printForm.Text = "Печать сводки по дому - " + CurrentHouse;

            // Инициализация
            printForm.Initialize(MainTable);

            Plugin.Host.MainWindow.AddPage(printForm);

            printForm.Show();
        }

        private void MainTableView_CustomColumnSort(object sender, DevExpress.XtraGrid.Views.Base.CustomColumnSortEventArgs e)
        {
            if(e.Column.FieldName == "ModulID")
            {
                OrdinalStringComparer comparer =  new OrdinalStringComparer();

                e.Result = comparer.Compare(MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex1, "ModulID").ToString(),
                                    MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex2, "ModulID").ToString());

                e.Handled = true;
            }

            if(e.Column.FieldName == "LastIntegratorData")
            {
                OrdinalStringComparer comparer = new OrdinalStringComparer();

                e.Result = comparer.Compare(MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex1, "LastIntegratorData").ToString(),
                                    MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex2, "LastIntegratorData").ToString());

                e.Handled = true;
            }

            if(e.Column.FieldName == "ConsumptionPer")
            {
                OrdinalStringComparer comparer = new OrdinalStringComparer();

                e.Result = comparer.Compare(MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex1, "ConsumptionPer").ToString(),
                                    MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex2, "ConsumptionPer").ToString());

                e.Handled = true;
            }

            if(e.Column.FieldName == "ConsumptionPerMounth")
            {
                OrdinalStringComparer comparer = new OrdinalStringComparer();

                e.Result = comparer.Compare(MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex1, "ConsumptionPerMounth").ToString(),
                                    MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex2, "ConsumptionPerMounth").ToString());

                e.Handled = true;
            }         

            if (e.Column.FieldName == "EquipmentChannel")
            {
                OrdinalStringComparer comparer = new OrdinalStringComparer();

                e.Result = comparer.Compare(MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex1, "EquipmentChannel").ToString(),
                                    MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex2, "EquipmentChannel").ToString());

                e.Handled = true;
            }
            
            if (e.Column.FieldName == "RoomNumber")
            {
                OrdinalStringComparer comparer = new OrdinalStringComparer();

                e.Result = comparer.Compare(MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex1, "RoomNumber").ToString(),
                                    MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex2, "RoomNumber").ToString());

                e.Handled = true;
            }

            if (e.Column.FieldName == "MeasurePointName")
            {
                OrdinalStringComparer comparer = new OrdinalStringComparer();

                e.Result = comparer.Compare(MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex1, "MeasurePointName").ToString(),
                                    MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex2, "MeasurePointName").ToString());

                e.Handled = true;
            }

            if (e.Column.FieldName == "EquipmentID")
            {
                OrdinalStringComparer comparer = new OrdinalStringComparer();

                e.Result = comparer.Compare(MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex1, "EquipmentID").ToString(),
                                    MainTableView.GetListSourceRowCellValue(e.ListSourceRowIndex2, "EquipmentID").ToString());

                e.Handled = true;
            }

        }
    }
}
