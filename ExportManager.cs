﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Windows.Forms;
using Lers.Core;
using Lers.Data;
using Lers.Communal;

namespace HouseSummary
{
    class ExportManager
    {
        /// <summary>
        /// Элемент управления для передачи вызовов в основной поток
        /// </summary>
        private Control parent;

        /// <summary>
		/// Событие завершения выполнения операции
		/// </summary>
		internal event CompletedEventHandler Completed;

        /// <summary>
		/// Делегат для вызова обработчика события Completed
		/// </summary>
		/// <param name="sender">Отправитель</param>
		/// <param name="e">Аргумент полученный в обработчике события RunWorkerCompleted объекта BackgroundWorker</param>
		internal delegate void CompletedEventHandler(object sender, RunWorkerCompletedEventArgs e);

        /// <summary>
		/// Делегат для вызова обработчика события ProgressChanged
		/// </summary>
		/// <param name="sender">Отправитель</param>
		/// <param name="e">Аргумент полученный в обработчике события ProgressChanged объекта BackgroundWorker</param>
		internal delegate void ProgressIncrementedEventHandler(object sender, ProgressChangedEventArgs e);

        /// <summary>
        /// Событие изменения прогресса операции
        /// </summary>
        internal event ProgressIncrementedEventHandler ProgressIncremented;

        /// <summary>
        /// Делегат для вызова обработчика события resultReceived
        /// </summary>
        /// <param name="sender">Отправитель</param>
        /// <param name="e">Аргумент для передачи информации о результате импорта</param>
        internal delegate void ResultReceivedEventHandler(object sender, ResultReceivedEventArgs e);

        /// <summary>
        /// Событие получения результата импорта архива
        /// </summary>
        internal event ResultReceivedEventHandler ResultReceived;

        IEnumerable<MeasurePoint> measurePointList;

        BindingList<TableRecord> dataList = new BindingList<TableRecord>();

        int avaliableData, consumptionForDate, consumptionFromDate;

        private BackgroundWorker backgroundWorker;

        public ExportManager(Control owner)
        {
            parent = owner;

            backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += new DoWorkEventHandler(backgroundWorker_DoWork);
            backgroundWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
            backgroundWorker.ProgressChanged += new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);

            backgroundWorker.WorkerReportsProgress = true;
            backgroundWorker.WorkerSupportsCancellation = true;
        }

        internal void Abort()
        {
            ResultReceived(this, new ResultReceivedEventArgs() { Message = "Экспорт отменён", records = dataList });

            backgroundWorker.CancelAsync();
        }

        internal void StopExporter()
        {
            backgroundWorker.CancelAsync();
        }

        private void backgroundWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (!parent.InvokeRequired)
            {
                // Поднимаем событие изменения прогресса выполнения операции
                if (ProgressIncremented != null)
                    ProgressIncremented(sender, e);
            }
            else
            {
                ProgressChangedEventHandler handler = new ProgressChangedEventHandler(backgroundWorker_ProgressChanged);
                parent.Invoke(handler, new object[] { sender, e });
            }
        }

        private void backgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // Проверяем требуется ли переход в основной поток
            if (!parent.InvokeRequired)
            {
                if (Completed != null)
                    Completed(sender, e);
            }
            else
            {
                // Запускаем метод в основном потоке
                RunWorkerCompletedEventHandler del = new RunWorkerCompletedEventHandler(backgroundWorker_RunWorkerCompleted);
                parent.Invoke(del, new object[] { sender, e });
            }
        }

        private void backgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            dataList.Clear();

            bool allOk = true;

            string exceptionMessage = "";

            foreach (MeasurePoint measurePoint in measurePointList)
            {
                try
                {
                    if (backgroundWorker.CancellationPending)
                    {
                        e.Cancel = true;
                        return;
                    }

                    TableRecord newRecord = CreateNewRecord(measurePoint);

                    if(newRecord.ConsumptionPer != "Не водяной счётчик")
                        dataList.Add(newRecord);

                    backgroundWorker.ReportProgress(1);
                }

                catch(Exception exc)
                {
                    allOk = false;

                    exceptionMessage += string.Format("{0}: {1} \r\n",  CompareTitle(measurePoint.FullTitle), ExceptionHandler.HandleException(this, exc));
                    dataList.Add(CreateEmptyRecord(measurePoint));

                    backgroundWorker.ReportProgress(1);
                    continue;
                }
            }

            if (ResultReceived != null)
                if (allOk)
                    ResultReceived(this, new ResultReceivedEventArgs() { Message = "Успех", records = dataList });     
                else
                {
                    ResultReceived(this, new ResultReceivedEventArgs() { Message = exceptionMessage, records = dataList });
                }      
        }

        private TableRecord CreateEmptyRecord(MeasurePoint measurePoint)
        {
            return new TableRecord(CompareTitle(measurePoint.FullTitle), "", "", "", "", "", "",
                    "", "", "", "", "");
        }
        private TableRecord CreateNewRecord(MeasurePoint measurePoint)
        {
            string measurePointTitle = CompareTitle(measurePoint.FullTitle); ;              

            string roomNumber = measurePointTitle.Split('-')[0];

            string autoPoll = measurePoint.AutoPoll.CommLinkType.ToString();

            string dataAvailable = IsAvailableDataFor(measurePoint);

            string lastDate = GetLastDataDate(measurePoint);

            string lastIntegrator = CompareLastIntegratorData(measurePoint);

            string equipmentID = "";
            string chanel = "";
            string modulID = "";
            string battaryDate = "";

            if (measurePoint.Device != null)
            {
                equipmentID = measurePoint.Device.Id.ToString();

                chanel = GetMeasurePointChannel(measurePoint);

                modulID = "";
                battaryDate = "";
            }

            //measurePoint.Refresh(MeasurePointInfoFlags.Attributes);

            MeasurePointAttributeCollection attributeCollection = measurePoint.Attributes;

            foreach (KeyValuePair<string, string> attribute in attributeCollection)
            {
                if (attribute.Key == "SN_radiomodul")
                    modulID = attribute.Value;

                if (attribute.Key == "Date_battery_replace")
                    battaryDate = attribute.Value;
            }

            string consumptionFor = GetConsumptionFor(measurePoint);

            string consumptionFrom = GetConsumptionFrom(measurePoint);

            return new TableRecord(roomNumber, measurePointTitle, autoPoll, dataAvailable, lastDate, lastIntegrator, equipmentID,
                    chanel, modulID, battaryDate, consumptionFor, consumptionFrom);
        }

        private string GetConsumptionFrom(MeasurePoint measurePoint)
        {
            DateTime date = new DateTime(DateTime.Today.Year, DateTime.Today.Month, consumptionFromDate);

            return GetConsumption(measurePoint, date);
        }

        private string GetConsumptionFor(MeasurePoint measurePoint)
        {
            DateTime beginDate = DateTime.Today.AddDays( - consumptionForDate);

            return GetConsumption(measurePoint, beginDate);
        }

        private string GetConsumption(MeasurePoint measurePoint, DateTime date)
        {
            double? counter = 0;

            MeasurePointConsumptionRecordCollection consumptionCollection = measurePoint.Data.GetConsumption(date, DateTime.Today, DeviceDataType.Day);

            if (consumptionCollection == null || consumptionCollection.Count == 0)
                return "нет данных";
            
            if (consumptionCollection.ElementAt(0) is MeasurePointConsumptionRecordWater)
            {
                foreach (MeasurePointConsumptionRecordWater consumption in consumptionCollection)
                {
                    if (consumption != null && consumption.V_in != null)
                        counter += consumption.V_in;
                }

                return CompareDataLenght(counter.ToString());
            }

            return "Не водяной счётчик";
        }

        private string CompareTitle(string title)
        {
            string[] temp = title.Split('-');

            if (temp.Length == 0)
                return "";

            title = title.Remove(0, temp[0].Length + 2);

            return title;
        }

        private string CompareLastIntegratorData(MeasurePoint measurePoint)
        {
            string lastIntegrator = GetLastIntegratorData(measurePoint);

            if (!(lastIntegrator.CompareTo("") == 0))
            {
                lastIntegrator = CompareDataLenght(lastIntegrator);

                return lastIntegrator;
            }
            else
                return "нет данных";
        }

        private static string CompareDataLenght(string data)
        {
            string[] temp = data.Split(',');

            if (temp.Length > 1)
            {
                if (temp[1].Length > 3)
                    temp[1] = temp[1].Remove(3);

                data = string.Format("{0},{1}", temp[0], temp[1]);

                return data;
            }

            if (data.CompareTo("0") == 0)
                return "0";

            return temp[0].ToString();
        }

        public void Export(IEnumerable<MeasurePoint> measurePointList, int avaliableData, int consumptionForDate, int consumptionFromDate)
        {            
            this.measurePointList = measurePointList;
            this.avaliableData = avaliableData;
            this.consumptionForDate = consumptionForDate;
            this.consumptionFromDate = consumptionFromDate;

            backgroundWorker.RunWorkerAsync();
        }

        private string GetMeasurePointChannel(MeasurePoint measurePoint)
        {
            measurePoint.Device.Refresh(EquipmentInfo.Bindings);

            DeviceCell[] equipmentChannels = measurePoint.Device.Bindings.Cells;

            if (equipmentChannels == null)
                return "Нет каналов";

            foreach (DeviceCell cell in equipmentChannels)
            {
                if (cell.MeasurePoint == measurePoint)
                    return cell.Cell.Name;
            }

            return "";
        }

        private string IsAvailableDataFor(MeasurePoint measurePoint)
        {
            DateTime date = DateTime.Today.AddDays(-avaliableData);

            MeasurePointConsumptionRecordCollection recordCollection = measurePoint.Data.GetConsumption(date, DateTime.Today.AddDays(-1), DeviceDataType.Day);

            DateTime lastRecordDate = new DateTime();

            foreach (MeasurePointConsumptionRecord record in recordCollection)
            {
                if (record.DateTime == date)
                {
                    date = date.AddDays(1);
                    lastRecordDate = record.DateTime;
                }

                else
                {
                    return "Нет";
                }

            }

            if (lastRecordDate != DateTime.Today.AddDays(-1) && lastRecordDate == new DateTime())
            {
                return "Нет";
            }

            return "Да";
        }

        private string GetLastIntegratorData(MeasurePoint measurePoint)
        {
            MeasurePointLastTotalsRecord record = measurePoint.Data.GetLastTotals();

            if (record == null || record.V_in == null)
                return "";

            return record.V_in.ToString();
        }

        private string GetLastDataDate(MeasurePoint measurePoint)
        {
            MeasurePointLastConsumptionRecord record = measurePoint.Data.GetLastConsumption();

            if (record == null)
                return "нет данных";

            return record.DateTime.ToString("dd MMM yyyy HH:mm:ss");
        }
    }
}
