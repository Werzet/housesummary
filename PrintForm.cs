﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DevExpress.XtraGrid;
using DevExpress.XtraPrinting;
using System.Drawing.Printing;

namespace HouseSummary
{
    public partial class PrintForm : DevExpress.XtraEditors.XtraForm
    {
        public PrintForm()
        {
            InitializeComponent();
        }

        internal void Initialize(GridControl mainTable)
        {
            printableComponentLink1.Component = mainTable;
        }
    }
}
