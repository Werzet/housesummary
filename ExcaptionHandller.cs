﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace HouseSummary
{
    public class ExceptionHandler
    {
        /// <summary>
        /// Строит сообщение исключения.
        /// </summary>
        /// <param name="exc">Исключение.</param>
        /// <returns>Сообщение об ошибке.</returns>
        private static string OutputExceptionMessage(Exception exc)
        {
            if (exc == null)
                return null;
            else
                return exc.Message + " " + OutputExceptionMessage(exc.InnerException);
        }

        /// <summary>
        /// Рекурсивный метод по разбору вложенного исключения. 
        /// </summary>
        /// <param name="exc">Исключение.</param>
        /// <returns>Стек вызовов.</returns>
        public static string OutputStackTrace(Exception exc)
        {
            if (exc == null)
                return null;
            else
                return exc.StackTrace + "\r\n=============================\r\n" + OutputStackTrace(exc.InnerException);
        }

        public static string HandleException(object sender, Exception exc)
        {
            return HandleException(null, new UnhandledExceptionEventArgs(exc, false));
        }

        /// <summary>
        /// Обрабатывает исключение.
        /// </summary>
        /// <param name="exc">Исключение.</param>
        public static string HandleException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception exc = (Exception)e.ExceptionObject;

            string message = "";

            message = string.Format("{0}. Стек исключения: {1}",
                                    OutputExceptionMessage(exc),
                                    OutputStackTrace(exc));

            return message;

        }
    }
}
